<?php

namespace Drupal\more_tables\Form;

define('FORM_KEYS', ['select', 'PRIMARY KEY', 'UNIQUE KEY', 'INDEX']);
define('FORM_TYPES', ['CHAR', 'VARCHAR', 'INT', 'SERIAL',
  'BIGINT', 'TINYINT', 'SMALLINT', 'DATE', 'DATETIME', 'TIME',
  'TIMESTAMP', 'MEDIUMINT', 'TEXT', 'BLOB', 'ENUM',
]
);

use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\more_tables\MoreTable;
use Drupal\more_tables\Utility\MoreUtils;

/**
 * Implements a Moretables form.
 */
class MoreTablesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'more_tables_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    // Gather the number of names in the form already.
    $num_names = $form_state->get('num_names') ?: NULL;
    // We have to ensure that there is at least one name field.
    if ($num_names === NULL) {
      $num_names = 1;
    }
    $form['table_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Table Name'),
      '#size' => 40,
      '#ajax' => [
        'callback' => [$this, 'validateTableName'],
        'event' => 'change',
        'wrapper' => 'main-container',
        'disable-refocus' => FALSE,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying Table...'),
        ],
      ],
      '#suffix' => '<span class="table-valid-message"></span>'
    ];
    $form['table_prefix'] = [
      '#type' => 'checkboxes',
      '#options' => ['yes' => 'Yes'],
      '#description' => $this->t('This will add custom prefix with your table name.'),
    ];
    $form['more_tables'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field Name'),
        $this->t('Type'),
        $this->t('Key'),
        $this->t('Length'),
        $this->t('Default Value'),
        $this->t('Is Null'),
        $this->t('Auto Increment'),
      ],
      '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'mytable-order-weight',
          ],
      ],
      '#prefix' => '<div id="main-container">',
      '#suffix' => '</div>',
    ];
    for ($key = 0; $key <= $num_names; ++$key) {
      $form['more_tables'][$key]['#attributes']['class'][] = 'draggable';
      $form['more_tables'][$key]['field_name'] = [
        '#type' => 'textfield',
        '#size' => 20,
      ];
      $form['more_tables'][$key]['field_type'] = [
        '#type' => 'select',
        '#options' => array_combine(FORM_TYPES, FORM_TYPES),
      ];
      $form['more_tables'][$key]['field_key'] = [
        '#type' => 'select',
        '#options' => array_combine(FORM_KEYS, FORM_KEYS),
      ];
      $form['more_tables'][$key]['field_length'] = [
        '#type' => 'number',
        '#size' => 20,
      ];
      $form['more_tables'][$key]['default_value'] = [
        '#type' => 'textfield',
        '#size' => 20,
      ];
      $form['more_tables'][$key]['is_null'] = [
        '#type' => 'checkboxes',
        '#options' => ['no' => 'NO'],
      ];
      $form['more_tables'][$key]['auto_increment'] = [
        '#type' => 'checkboxes',
        '#options' => ['yes' => 'YES'],
      ];
    }

    $form['more_tables']['actions'] = [
      '#type' => 'actions',
    ];
    $form['more_tables']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
      '#button_type' => 'primary',
    ];
    $form['more_tables']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => 'Add another',
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'main-container',
      ],
    ];
    if ($num_names > 1) {
      $form['more_tables']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'main-container',
        ],
      ];
    }

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['more_tables'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    $add_button = $name_field + 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

   /**
   * {@inheritdoc}
   */
  public function validateTableName(array &$form, FormStateInterface $form_state) {
    $utils = new MoreUtils();
    $valid = $utils->getSchemaBasedOnTableName($form_state->getValue('table_name'));
    $response = new AjaxResponse();
    if (empty($valid)) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Valid table name.');
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Table already exists.');
    }
    $response->addCommand(new CssCommand('#edit-table-name', $css));
    $response->addCommand(new HtmlCommand('.table-valid-message', $message));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    unset($form_state->getValue('more_tables')['actions']);
    $obj = new MoreTable();
    if ($form_state->getValue('table_prefix')['yes'] === 'yes') {
      $table_name = 'custom_' . $form_state->getValue('table_name');
    }
    else {
      $table_name = $form_state->getValue('table_name');
    }
    $obj->setTableName($table_name);
    $fields = [];
    foreach ($form_state->getValues()['more_tables'] as $value) {
      $fields[$value['field_name']] = $value;
      unset($fields[$value['field_name']]['field_name']);
    }
    $obj->setFields($fields)->prepareTableData();
  }

}
