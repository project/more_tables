<?php

namespace Drupal\more_tables\Form;

define('MORE_TABLES_KEYS', ['select', 'PRIMARY KEY', 'FOREIGN KEY']);
define('MORE_TABLES_TYPES', ['CHAR', 'VARCHAR', 'INT',
  'BIGINT', 'TINYINT', 'SMALLINT', 'DATE', 'DATETIME', 'TIME',
  'TIMESTAMP', 'MEDIUMINT', 'TEXT', 'BLOB', 'ENUM',
]
);

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\more_tables\MoreTable;
use Drupal\more_tables\Utility\MoreUtils;

/**
 * Implements an DropTruncateTable form.
 */
class DropTruncateTableForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drop_truncate_table_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $utils = new MoreUtils();
    $form['#tree'] = TRUE;
    $form['table_name'] = [
      '#type' => 'select',
      '#options' => array_combine($utils->getAllTables(), $utils->getAllTables()),
      '#title' => $this->t('Table Name'),
    ];
    $form['table_ops'] = [
      '#type' => 'checkboxes',
      '#options' => ['truncate' => 'TRUNCATE'],
      '#description' => $this->t('Select this for truncate the table by default its perform DROP operation.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $obj = new MoreTable();
    $obj->setTableName($form_state->getValue('table_name'));
    if ($form_state->getValue('table_ops')['truncate'] === 'truncate') {
      $obj->truncateTable();
    }
    else {
      $obj->dropTable();
    }
  }

}
