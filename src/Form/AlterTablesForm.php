<?php

namespace Drupal\more_tables\Form;

define('KEYS', ['select', 'PRIMARY KEY', 'UNIQUE KEY', 'INDEX']);
define(
    'TYPES', ['CHAR', 'VARCHAR', 'INT',
      'BIGINT', 'TINYINT', 'SMALLINT', 'DATE', 'DATETIME', 'TIME',
      'TIMESTAMP', 'MEDIUMINT', 'TEXT', 'BLOB', 'ENUM',
    ]
);

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\more_tables\MoreTable;
use Drupal\more_tables\Utility\MoreUtils;

/**
 * {@inheritdoc}
 */
class AlterTablesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->utils = new MoreUtils();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alter_tables_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    // Gather the number of names in the form already.
    $num_names = $form_state->get('num_names') ?: NULL;
    // We have to ensure that there is at least one name field.
    if ($num_names === NULL) {
      $num_names = $form_state->set('num_names', 1);
    }
    $form['table_name'] = [
      '#type' => 'select',
      '#options' => ['--Select Table--'] + array_combine($this->utils->getAllTables(), $this->utils->getAllTables()),
      '#title' => $this->t('Table Name'),
      '#ajax' => [
        'callback' => [$this, 'selectTblNameCallback'],
        'event' => 'change',
        'wrapper' => 'main-container',
        'disable-refocus' => FALSE,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Fetching Data...'),
        ],
      ],
    ];
    $form['rename_table'] = [
      '#type' => 'checkboxes',
      '#options' => ['yes' => 'Yes'],
      '#title' => $this->t('Rename Table'),
      '#description' => $this->t('This will change your table name.'),
      '#attributes' => [
        'name' => 'rename_more_table',
      ],
    ];
    $form['new_table_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New Table Name'),
      '#size' => 40,
      '#states' => [
        'visible' => [
          ':input[name=rename_more_table]' => [
            'checked' => TRUE,
          ],
        ],
        'required' => [
          ':input[name=rename_more_table]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['alter_tables'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field Name'),
        $this->t('Type'),
        $this->t('Key'),
        $this->t('Length'),
        $this->t('Default Value'),
        $this->t('Is Null'),
        $this->t('Auto Increment'),
      ],
      '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'mytable-order-weight',
          ],
      ],
      '#prefix' => '<div id="main-container">',
      '#suffix' => '</div>',
    ];
    for ($key = 0; $key <= $num_names; ++$key) {
      $form['alter_tables'][$key]['field_name'] = [
        '#type' => 'textfield',
        '#size' => 20,
      ];
      $form['alter_tables'][$key]['field_type'] = [
        '#type' => 'select',
        '#options' => array_combine(TYPES, TYPES),
      ];
      $form['alter_tables'][$key]['field_key'] = [
        '#type' => 'select',
        '#options' => array_combine(KEYS, KEYS),
      ];
      $form['alter_tables'][$key]['field_length'] = [
        '#type' => 'number',
        '#size' => 20,
      ];
      $form['alter_tables'][$key]['default_value'] = [
        '#type' => 'textfield',
        '#size' => 20,
      ];
      $form['alter_tables'][$key]['is_null'] = [
        '#type' => 'select',
        '#options' => ['yes' => 'YES', 'no' => 'NO'],
      ];
      $form['alter_tables'][$key]['auto_increment'] = [
        '#type' => 'select',
        '#options' => ['no' => 'NO', 'yes' => 'YES'],
      ];
    }

    $form['alter_tables']['actions'] = [
      '#type' => 'actions',
    ];
    $form['alter_tables']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['alter_tables']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => 'Add another',
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'main-container',
      ],
    ];

    if ($num_names > 1) {
      $form['alter_tables']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'main-container',
        ],
      ];
    }

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['alter_tables'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    $add_button = $name_field + 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    unset($form_state->getValue('alter_tables')['actions']);
    $process = '';
    $new_table_name = $form_state->getValue('new_table_name');
    $table_name = $form_state->getValue('table_name');
    $more_table = new MoreTable();
    $process = (!empty($new_table_name))
    ? $more_table->setTableName($new_table_name)->renameTable($table_name)
    : $more_table->setTableName($table_name);

    $fields = [];
    foreach ($form_state->getValues()['alter_tables'] as $value) {
      $fields[$value['field_name']] = $value;
      unset($fields[$value['field_name']]['field_name']);
    }

    $more_table->setFields($fields)->alterTable();
  }

  /**
   * {@inheritdoc}
   */
  public function selectTblNameCallback(array &$form, FormStateInterface $form_state) {
    // Return the element that will replace the wrapper (we return itself).
    $response = $this->utils->getSchemaBasedOnTableName($form_state->getValue('table_name'));
    foreach ($response as $key => $value) {
      $form['alter_tables'][$key]['field_name']['#value'] = $value['field_name'];
      $form['alter_tables'][$key]['field_type']['#value'] = $value['field_type'];
      $form['alter_tables'][$key]['field_key']['#value'] = $value['field_key'];
      $form['alter_tables'][$key]['field_length']['#value'] = $value['field_length'];
      $form['alter_tables'][$key]['default_value']['#value'] = $value['default_value'];
      $form['alter_tables'][$key]['is_null']['#value'] = strtolower($value['is_null']);
      $form['alter_tables'][$key]['auto_increment']['#value'] = strtolower($value['auto_increment']);
    }
    $form['alter_tables']['num_names'] = count($response);
    $form_state->setRebuild(TRUE);

    return $form['alter_tables'];
  }

}
