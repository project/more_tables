<?php

namespace Drupal\more_tables;

use Drupal\more_tables\Utility\MoreUtils;

/**
 *
 */
class MoreTable extends MoreUtils {

  /**
   * Fields of table.
   *
   * @var array
   */
  protected $fields = NULL;

  /**
   * Table name.
   *
   * @var mixed
   */
  protected $table_name = NULL;

  /**
   * Set fields of the table.
   *
   * @param array $fields
   *   Collection of fields.
   */
  public function setFields($fields) {
    $this->fields = $fields;
    return $this;
  }

  /**
   * Get fields of the table.
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * Set table name.
   *
   * @param mixed $table_name
   *   Table name.
   */
  public function setTableName($table_name) {
    $this->table_name = $table_name;
    return $this;
  }

  /**
   * Get table name.
   */
  public function getTableName() {
    return $this->table_name;
  }

  /**
   * Function to prepare table.
   */
  public function prepareTableData() {
    $statement = [];
    $prepare = '';
    foreach ($this->fields as $key => $value) {
      $length = $is_null = $auto_inc = '';
      $default = $value['default_value'];
      if (!empty($value['field_length'])) {
        $length = "(" . $value['field_length'] . ")";
      }
      if (!empty($value['default_value']) && is_string($value['default_value'])) {
        $default = "DEFAULT '" . $value['default_value'] . "'";
      }
      if (implode(',', $value['is_null']) == 'no') {
        $is_null = "NOT NULL";
      }
      if (implode(',', $value['auto_increment']) == 'yes') {
        $auto_inc = "AUTO_INCREMENT";
      }
      $statement[] = $key . ' ' . strtoupper($value['field_type']) . $length . ' ' . $default . ' ' . $is_null . ' ' . $auto_inc;

      if ($value['field_key'] == 'PRIMARY KEY') {
        $primary_key[] = $value['field_key'] . "( $key )";
      }
      elseif ($value['field_key'] == 'UNIQUE KEY') {
        $unique_key[] = $value['field_key'] . "( $key )";
      }
      elseif ($value['field_key'] == 'INDEX') {
        $index[] = $value['field_key'] . "( $key )";
      }
    }
    $prepare .= implode(",", $statement) . ",\n";
    if (!empty($primary_key) && !empty($unique_key) && !empty($index)) {
      $prepare .= implode(",", $primary_key) . ",\n";
      $prepare .= implode(",", $unique_key) . ",\n";
      $prepare .= implode(",", $index) . " \n";
    }
    elseif (!empty($primary_key) && !empty($unique_key)) {
      $prepare .= implode(",", $primary_key) . ",\n";
      $prepare .= implode(",", $unique_key) . " \n";
    }
    elseif (!empty($primary_key) && !empty($index)) {
      $prepare .= implode(",", $primary_key) . ",\n";
      $prepare .= implode(",", $index) . " \n";
    }
    elseif (!empty($unique_key) && !empty($index)) {
      $prepare .= implode(",", $unique_key) . ",\n";
      $prepare .= implode(",", $index) . " \n";
    }
    $this->createTable($prepare);
  }

  /**
   * Function to create table.
   *
   * @param array $definition
   *   Schema definition.
   *
   * @return bool
   *   TRUE/FALSE.
   */
  public function createTable($definition) {
    $query = "CREATE TABLE IF NOT EXISTS $this->table_name ( \n $definition )";
    try {
      $table = \Drupal::database()->query($query);
      return TRUE;
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }

  }

  /**
   * Function to delete the table.
   */
  public function dropTable() {
    $query = "DROP TABLE $this->table_name";
    try {
      $table = \Drupal::database()->query($query);
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

  /**
   * Function to truncate the table.
   */
  public function truncateTable() {
    $query = "TRUNCATE TABLE $this->table_name";
    try {
      $table = \Drupal::database()->query($query);
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

  /**
   * Function to prepare data for alter the table.
   */
  public function alterTable() {
    $schema = $this->getSchemaBasedOnTableName($this->table_name);
    $diff = array_diff(array_column($schema, 'field_name'), array_keys($this->fields));
    if (count($this->fields) == count($schema)) {
      $definition = [];
      $i = 0;
      foreach ($this->fields as $key => $field) {
        $length = (!empty($field['field_length']))
        ? '( ' . $field['field_length'] . ' )'
        : $field['field_length'];
        $is_null = (array_values($field['is_null'])[0] === 'no') ? 'NOT NULL' : 'NULL';
        $default = "DEFAULT '" . $field['default_value'] . "'";
        $field_key = $field['field_key'];
        if (!in_array($key, array_column($schema, 'field_name'))) {
          $definition[] = 'CHANGE COLUMN ' . $schema[$i]['field_name'] . ' ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $default . ' ' . $is_null;
        }
        else {
          $definition[] = 'MODIFY COLUMN ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $is_null . ' ' . $field_key;
        }
        $i++;
      }
      $this->performAlterOperation($definition);
    }
    elseif (count($this->fields) > count($schema)) {
      $definition = [];
      $i = 0;
      foreach ($this->fields as $key => $field) {
        // Echo array_values($field['is_null']); die;.
        $length = (!empty($field['field_length']))
        ? '( ' . $field['field_length'] . ' )'
        : $field['field_length'];
        $is_null = (array_values($field['is_null'])[0] === 'no') ? 'NOT NULL' : 'NULL';
        $default = "DEFAULT '" . $field['default_value'] . "'";
        $field_key = $field['field_key'];
        if (!in_array($key, array_column($schema, 'field_name'))) {
          $definition[] = 'CHANGE COLUMN ' . $schema[$i]['field_name'] . ' ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $default . ' ' . $is_null;
        }
        else {
          $definition[] = 'MODIFY COLUMN ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $default . ' ' . $is_null . ' ' . $field_key;

        }
        $i++;
      }
      $this->performAlterOperation($definition);
    }
    elseif (count($this->fields) < count($schema)) {
      foreach ($diff as $value) {
        $definition[] = 'DROP COLUMN ' . $value;
      }
      $this->performAlterOperation($definition);
      $i = 0;
      foreach ($this->fields as $key => $field) {
        // Echo array_values($field['is_null']); die;.
        $length = (!empty($field['field_length']))
        ? '( ' . $field['field_length'] . ' )'
        : $field['field_length'];
        $is_null = (array_values($field['is_null'])[0] === 'no') ? 'NOT NULL' : 'NULL';
        $default = "DEFAULT '" . $field['default_value'] . "'";
        $field_key = $field['field_key'];
        if (!in_array($key, array_column($schema, 'field_name'))) {
          $definition[] = 'ADD ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $default . ' ' . $is_null . ' ' . $field_key;
        }
        else {
          $definition[] = 'MODIFY COLUMN ' . $key . ' ' . $field['field_type'] . ' ' . $length . ' ' . $default . ' ' . $is_null . ' ' . $field_key;

        }
        $i++;
      }
      $this->performAlterOperation($definition);
    }
  }

  /**
   * Function to rename the table name.
   *
   * @param mixed $table_name
   *   Table name.
   */
  public function renameTable($tbl_name) {
    $operation = '';
    $query = "ALTER TABLE $tbl_name RENAME TO $this->table_name";
    try {
      $operation = \Drupal::database()->query($query);
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

  /**
   * Function to rename the table name.
   *
   * @param mixed $table_name
   *   Table name.
   */
  public function renameColumn($column, $new_column, $definition) {
    $operation = '';
    $query = "ALTER TABLE $this->table_name CHANGE COLUMN $column $new_column $definition";
    try {
      $operation = \Drupal::database()->query($query);
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

  /**
   * Function to perform table alter operation.
   *
   * @param array $definition
   *   Schema definition.
   */
  public function performAlterOperation($definition) {
    $operation = '';
    $statement = implode(',', $definition);
    $query = "ALTER TABLE $this->table_name \n $statement";
    try {
      $operation = \Drupal::database()->query($query);
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

}
