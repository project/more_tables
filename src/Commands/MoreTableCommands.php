<?php

namespace Drupal\more_tables\Commands;

use Drush\Commands\DrushCommands;
use Drupal\more_tables\MoreTable;

/**
 * A drush command file.
 */
class MoreTableCommands extends DrushCommands {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->moreTable = new MoreTable();
  }

  /**
   * Drush command that drop the given table from your database.
   *
   * @param string $table_name
   *   Argument with message to be displayed.
   *
   * @command more-table:delete
   * @aliases mt-drop,mtd
   * @usage more-table:delete table_name
   */
  public function deleteTable($table_name = NULL) {
    $message = 'Table not found';
    if (!empty($table_name)) {
      if ($this->confirm("Are you sure you want delete $table_name to the screen ?")) {
        $this->moreTable->setTableName($table_name)->dropTable();
        $message = "$table_name is deleted successfully.";
        drush_print($message);
      }
      else {
        throw new UserAbortException();
      }
    }
  }

  /**
   * Drush command that truncate the given table from your database.
   *
   * @param string $table_name
   *   Argument with message to be displayed.
   *
   * @command more-table:truncate
   * @aliases mt-truncate,mttr
   * @usage more-table:truncate table_name
   */
  public function truncate($table_name = NULL) {
    $message = 'Table not found';
    if (!empty($table_name)) {
      if ($this->confirm("Are you sure you want truncate this table with name $table_name ?")) {
        $this->moreTable->setTableName($table_name)->truncateTable();
        $message = "$table_name is truncate successfully.";
        drush_print($message);
      }
      else {
        throw new UserAbortException();
      }
    }
  }

  /**
   * Drush command that rename the given table from your database.
   *
   * @param string $table_name
   *   Actual table name.
   * @param string $new_table_name
   *   New table name.
   *
   * @command more-table:rename
   * @aliases mt-rename,mtr
   * @usage more-table:rename table_name new_table_name
   */
  public function rename($table_name = NULL, $new_table_name = NULL) {
    $message = 'Table not found';
    if (!empty($table_name) && !empty($new_table_name)) {
      if ($this->confirm("Are you sure you want rename the table with name $table_name ?")) {
        $this->moreTable->setTableName($new_table_name)->renameTable($table_name);
        $message = "$table_name is renamed as $new_table_name successfully.";
        drush_print($message);
      }
      else {
        throw new UserAbortException();
      }
    }
  }

  /**
   * Drush command that rename the column name in the table.
   *
   * @param string $table_name
   *   Actual table name.
   *
   *   $option column
   *   Column name of the table.
   *
   * @option new_column
   *   New column name of the table.
   * @option type
   *   Column type.
   * @option length
   *   Length of the column.
   * @command more-table:changeColumn
   * @aliases mt-change-col,mtcc
   * @usage more-table:changeColumn table_name --column=column_name --new_column=new_column_name --type=int --length=10
   */
  public function changeColumn($table_name, $options = ['column' => NULL, 'new_column' => NULL, 'type' => NULL, 'length' => NULL]) {
    $message = 'Table not found';
    $col = $options['column'];
    $new_col = $options['new_column'];
    if (!empty($table_name) && !empty($col) && !empty($new_col)) {
      $definition = $options['type'] . '( ' . $options['length'] . ' )';
      if ($this->confirm("Are you sure you want rename the $col to $new_col ?")) {
        $this->moreTable->setTableName($table_name)->renameColumn($col, $new_col, $definition);
        $message = "$col is changed to $new_col successfully.";
        drush_print($message);
      }
      else {
        throw new UserAbortException();
      }
    }
  }

}
