<?php

namespace Drupal\more_tables\Utility;

use Drupal\Core\Database\Database;

/**
 * Provides integration with More table.
 */
class MoreUtils {

  /**
   * Function to retrieve schema of the table.
   *
   * @param mixed $tbl_name
   *   Table Name.
   *
   * @return array
   *   Schema array
   */
  public function getSchemaBasedOnTableName($tbl_name) {
    try {
      $result = \Drupal::database()->query("SHOW FULL COLUMNS FROM $tbl_name")->fetchAll();
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
    foreach ($result as $key => $value) {
      if ($value->Key === 'PRI') {
        $field_key = 'PRIMARY KEY';
      }
      elseif ($value->Key === 'MUL') {
        $field_key = 'INDEX';
      }
      elseif ($value->Key === 'UNI') {
        $field_key = 'UNIQUE KEY';
      }
      else {
        $field_key = 'select';
      }
      $type = explode('(', $value->Type);
      $output[$key]['field_name'] = $value->Field;
      $output[$key]['field_key'] = $field_key;
      $output[$key]['field_type'] = strtoupper($type[0]);
      $output[$key]['field_length'] = explode(')', $type[1])[0];
      $output[$key]['default_value'] = $value->Default;
      $output[$key]['is_null'] = strtoupper($value->Null);
      $output[$key]['auto_increment'] = !empty($value->Extra) ? 'YES' : '';
    }

    return $output;
  }

  /**
   * Function to retrieve all tables in the database.
   *
   * @return array
   *   List of table
   */
  public function getAllTables() {
    try {
      $db_connection = Database::getConnection('default', 'default');
      $db_name = "'" . $db_connection->getConnectionOptions()['database'] . "'";
      $result = \Drupal::database()->query("SELECT table_name as list FROM information_schema.tables
		WHERE table_schema = $db_name")->fetchCol();

      return $result;
    }
    catch (\Exception $e) {
      \Drupal::logger('more-table')->error($e->getMessage());
    }
  }

}
